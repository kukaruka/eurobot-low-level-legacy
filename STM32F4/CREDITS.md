Credits
=======

  - Eurobot 2022
     * [Maksim Katerishich](https://gitlab.com/kukaruka)
     * Iana Zhura

  - Eurobot 2021
     * Ildar Babataev
     * Nipun Dhananjaya

  - Eurobot 2020
     * [Nikita Mikhailovskiy](https://github.com/CityAplons)
        ¯\_(ツ)_/¯

   - Eurobot 2019
     * [Edgar Kaziakhmedov](https://github.com/edosedgar)
       Linux toolchain support and core contributor
     * [Nikita Gorbadey](https://github.com/nikigor)
       MacOS support and core contributor
     * [Juan Esteban Heredia Mena](https://github.com/JuanesHe)
       Documentation support

   - Eurobot 2018
     * [Sergei Vostrikov](https://github.com/Sergio5714)
       The first implementation of firmware
