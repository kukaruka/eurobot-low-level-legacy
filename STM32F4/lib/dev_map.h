#ifndef _DEV_MAP_H_
#define _DEV_MAP_H_

#define NUMBER_OF_DYNAMIXELS            9
#define PAWS_NUMBER 5
#define DOOR_NUMBER 4

#define NUMBER_OF_PROX_SENSORS          3
#define NUMBER_OF_STEP_MOTORS           1

#endif //_DEV_MAP_H_
