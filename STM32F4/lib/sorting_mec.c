#include <stdio.h>
#include <string.h>
#include "sorting_mec.h"
#include "terminal_cmds.h"
#include "gpio_map.h"
#include "peripheral.h"
#include "terminal.h"
#include "FreeRTOS.h"
#include "task.h"

static void MX_TIM12_Init(void)
{    /* Peripheral clock enable */

    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOC);
    LL_GPIO_SetPinMode(GPIOC, LL_GPIO_PIN_6,
                        LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetAFPin_0_7(GPIOC, LL_GPIO_PIN_6,
                            LED_PIN_AF);
    LL_GPIO_SetPinOutputType(GPIOC, LL_GPIO_PIN_6,
                                LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinPull(GPIOC,LL_GPIO_PIN_6,LL_GPIO_PULL_NO);
    LL_GPIO_SetPinSpeed(GPIOC,LL_GPIO_PIN_6,LL_GPIO_SPEED_FREQ_HIGH);

    LL_GPIO_SetPinMode(GPIOC, LL_GPIO_PIN_7,
                        LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetAFPin_0_7(GPIOC, LL_GPIO_PIN_7,
                            LED_PIN_AF);
    LL_GPIO_SetPinOutputType(GPIOC, LL_GPIO_PIN_7,
                                LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinPull(GPIOC,LL_GPIO_PIN_7,LL_GPIO_PULL_NO);
    LL_GPIO_SetPinSpeed(GPIOC,LL_GPIO_PIN_7,LL_GPIO_SPEED_FREQ_HIGH);

    LL_GPIO_SetPinMode(GPIOC, LL_GPIO_PIN_8,
                        LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetAFPin_8_15(GPIOC, LL_GPIO_PIN_8,
                            LED_PIN_AF);
    LL_GPIO_SetPinOutputType(GPIOC, LL_GPIO_PIN_8,
                                LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinPull(GPIOC,LL_GPIO_PIN_8,LL_GPIO_PULL_NO);
    LL_GPIO_SetPinSpeed(GPIOC,LL_GPIO_PIN_8,LL_GPIO_SPEED_FREQ_HIGH);

    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_TIM8);
    LL_TIM_EnableUpdateEvent(TIM8);
    LL_TIM_SetClockDivision(TIM8, LL_TIM_CLOCKDIVISION_DIV4);
    LL_TIM_SetCounterMode(TIM8, LL_TIM_COUNTERMODE_UP);
    LL_TIM_SetAutoReload(TIM8, 65535);
    LL_TIM_SetUpdateSource(TIM8, LL_TIM_UPDATESOURCE_REGULAR);

    // /* Enable capture mode */
    LL_TIM_CC_EnableChannel(TIM8, LL_TIM_CHANNEL_CH1 |
                            LL_TIM_CHANNEL_CH2 | LL_TIM_CHANNEL_CH3);

    /* Set PWM mode */
    LL_TIM_OC_SetMode(TIM8, LL_TIM_CHANNEL_CH1, LL_TIM_OCMODE_PWM1);
    LL_TIM_OC_SetMode(TIM8, LL_TIM_CHANNEL_CH2, LL_TIM_OCMODE_PWM1);
    LL_TIM_OC_SetMode(TIM8, LL_TIM_CHANNEL_CH3, LL_TIM_OCMODE_PWM1);

    /* Enable fast mode */
    LL_TIM_OC_EnableFast(TIM8, LL_TIM_CHANNEL_CH1);
    LL_TIM_OC_EnableFast(TIM8, LL_TIM_CHANNEL_CH2);
    LL_TIM_OC_EnableFast(TIM8, LL_TIM_CHANNEL_CH3);

    /* Enable preload */
    LL_TIM_OC_EnablePreload(TIM8, LL_TIM_CHANNEL_CH1);
    LL_TIM_OC_EnablePreload(TIM8, LL_TIM_CHANNEL_CH2);
    LL_TIM_OC_EnablePreload(TIM8, LL_TIM_CHANNEL_CH3);
    LL_TIM_EnableARRPreload(TIM8);

    LL_TIM_OC_EnableClear(TIM8,LL_TIM_CHANNEL_CH1);
    LL_TIM_OC_EnableClear(TIM8,LL_TIM_CHANNEL_CH2);
    LL_TIM_OC_EnableClear(TIM8,LL_TIM_CHANNEL_CH3);

    /* Disable advanced fetures of the timer*/
    LL_TIM_DisableMasterSlaveMode(TIM8);
    LL_TIM_CC_SetLockLevel(TIM8,LL_TIM_LOCKLEVEL_OFF);
    LL_TIM_OC_SetDeadTime(TIM8, 0);
    LL_TIM_DisableBRK(TIM8);
    LL_TIM_ConfigBRK(TIM8,LL_TIM_BREAK_POLARITY_HIGH);
    LL_TIM_SetOffStates(TIM8,LL_TIM_OSSI_DISABLE,LL_TIM_OSSR_DISABLE);
    LL_TIM_EnableAutomaticOutput(TIM8);

    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOC);
    LL_GPIO_SetPinMode(GPIOD, SORT_DC_MOTOR_P1, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinMode(GPIOD, SORT_DC_MOTOR_P2, LL_GPIO_MODE_OUTPUT);
    
}

void init_sort_mech(){
    sorting_mec.target_dist = 50;
    sorting_mec.step_mask = SORT_DC_MOTOR_P1 | SORT_DC_MOTOR_P2;
    sorting_mec.state[0] = SORT_DC_MOTOR_P1;
    sorting_mec.state[1] = SORT_DC_MOTOR_P2;
    sorting_mec.state[2] = 0;
    MX_TIM12_Init();
    LL_TIM_EnableIT_UPDATE(TIM8);
    NVIC_SetPriority(TIM8_UP_TIM13_IRQn, STEP_TIM_IRQN_PRIORITY - 2);
    NVIC_EnableIRQ(TIM8_UP_TIM13_IRQn);
    
    LL_TIM_SetAutoReload(TIM8, (uint32_t) STICK_TIM_ARR*2);
    LL_TIM_OC_EnableFast(TIM8, LL_TIM_CHANNEL_CH1);
    LL_TIM_SetPrescaler(TIM8, STICK_TIM_PSC);
    LL_TIM_OC_SetCompareCH1(TIM8, (uint32_t) STEP_TIM_CCR_INIT*0.336);
    LL_TIM_EnableCounter(TIM8);
    
    uint32_t port_state = LL_GPIO_ReadInputPort(GPIOD);
    port_state &= ~sorting_mec.step_mask;
    LL_GPIO_WriteOutputPort(GPIOD, port_state);
}

int cmd_set_sorting_dist(char *args)
{
    uint32_t port_state = LL_GPIO_ReadInputPort(GPIOD);
    port_state &= ~sorting_mec.step_mask;
    LL_GPIO_WriteOutputPort(GPIOD, port_state);

    LL_TIM_DisableCounter(TIM8);
    uint8_t distance = (uint8_t) args[0];
    if (distance < 10 || distance > 0xB5)
        goto error_sort_error;

    sorting_mec.target_dist = distance;
    uint8_t status = (uint8_t) args[1];
    switch (status) {
    case 0x00:
        LL_TIM_SetAutoReload(TIM8, (uint32_t) STICK_TIM_ARR*2);
        LL_TIM_OC_EnableFast(TIM8, LL_TIM_CHANNEL_CH1);
        LL_TIM_SetPrescaler(TIM8, STICK_TIM_PSC);
        LL_TIM_OC_SetCompareCH1(TIM8, (uint32_t) STEP_TIM_CCR_INIT*0.192);
        break;
    case 0x01:
        LL_TIM_SetAutoReload(TIM8, (uint32_t) STICK_TIM_ARR*2);
        LL_TIM_OC_EnableFast(TIM8, LL_TIM_CHANNEL_CH1);
        LL_TIM_SetPrescaler(TIM8, STICK_TIM_PSC);
        LL_TIM_OC_SetCompareCH1(TIM8, (uint32_t) STEP_TIM_CCR_INIT*0.336);
        break;
    default:
        goto error_sort_error;
        break;
    }
    LL_TIM_EnableCounter(TIM8);
    return "OK";
    
error_sort_error:
        memcpy(args, "ER", 3);
        return 3;
}

void TIM8_UP_TIM13_IRQHandler(void)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    LL_TIM_ClearFlag_UPDATE(TIM8);
    uint32_t port_state = LL_GPIO_ReadInputPort(GPIOD);
    port_state &= ~sorting_mec.step_mask;
    cmd_get_col_av_data(NULL);
    if (sorting_mec.current_dist < 15){
        goto error_current_dist;
    }
    if (sorting_mec.target_dist - sorting_mec.current_dist > ALLOWED_DELTA){
            port_state |= sorting_mec.state[0];
    } else if (sorting_mec.target_dist - sorting_mec.current_dist < -ALLOWED_DELTA){
            port_state |= sorting_mec.state[1];
    }

error_current_dist:
    LL_GPIO_WriteOutputPort(GPIOD, port_state);
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}