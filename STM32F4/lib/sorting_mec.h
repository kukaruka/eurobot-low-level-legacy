#ifndef _SORTING_MEC_
#define _SORTING_MEC_
#include <stdint.h>
#include "stm32f407xx.h"
#include "stm32f4xx_ll_gpio.h"
#include "terminal.h"

#define ALLOWED_DELTA 5
#define SORT_DC_MOTOR_P1                       LL_GPIO_PIN_6
#define SORT_DC_MOTOR_P2                       LL_GPIO_PIN_7

typedef struct{
   uint16_t current_dist;
   uint16_t target_dist;
   uint16_t step_mask;
   uint16_t state[3];
}sorting_mec_t;

sorting_mec_t sorting_mec;
/*
 * Memory for terminal task
 */
// #define SORTING_MEC_STACK_DEPTH    1024
// StackType_t sorting_mec_ts[SORTING_MEC_STACK_DEPTH];
// StaticTask_t sorting_mec_tb;


void init_sort_mech();

#endif 