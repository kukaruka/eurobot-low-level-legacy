import serial
from threading import Lock
import struct
import time
import itertools
import pandas as pd

coord_dict = {}
speed_dict = {}
wheel_dict = {}

def dict_append(var, val, dic):
    if var in dic:
        dic[var].append(val)
    else:
        dic[var] = [val]

class STMprotocol(object):
    def __init__(self, serial_port, baudrate=115200):

        self.mutex = Lock()

        self.ser = serial.Serial(serial_port, baudrate=baudrate, timeout=0.01)
        self.robot_name = "main_robot"
        if self.robot_name == "main_robot":  # added new bytes fro 0x2.
            self.pack_format = {
                0x01: "=cccc",
                0x02: "=",
                0x03: "=",
                0x04: "=",
                0x05: "=",
                0x07: "=",
                0x08: "=fff",
                0x09: "=",
                0x0e: "=fff",
                0x0f: "=",
                0x10: "=B",
                0x11: "=B",
                0x12: "=B",
                0x13: "=B",
                0x14: "=B",
                0x15: "=B",
                0x16: "=BB",
                0x17: "=",
                0x18: "=",
                0x19: "=",
                0x1A: "=",
                0x1B: "=",
                0x20: "=",
                0x21: "=",
                0x22: "=",
                0x23: "=",
                0x24: "=",
                0x25: "=",
                0x26: "=",
                0x27: "=",
                0x28: "=",
                0x29: "=",
                0x2A: "=",
                0x2B: "=",
                0x2C: "=",
                0x2D: "=",
                0x2E: "=",
                0x2F: "=",
                0x30: "=",
                0x31: "=",
                0x32: "=",
                0x33: "=",
                0x34: "=",
                0x35: "=",
                0x36: "=",
                0x37: "=",

                0x60: "=",
                0x61: "=BB",
                0x62: "=",
                0x63: "=",
                0x64: "=",

                0x70: "=",
            }

            self.unpack_format = {
                0x01: "=cccc",
                0x02: "=cc",
                0x03: "=B",
                0x04: "=B",
                0x05: "=B",
                0x07: "=fff",
                0x08: "=cc",
                0x09: "=fff",
                0x0e: "=cc",
                0x0f: "=fff",

                0x10: "=cc",
                0x11: "=cc",
                0x12: "=cc",
                0x13: "=cc",
                0x14: "=cc",
                0x15: "=cc",
                0x16: "=cc",
                0x17: "=cc",
                0x18: "=cc",
                0x19: "=cc",  # fixed 18.12
                0x1A: "=cc",
                0x1B: "=cc",  # fixed 18.12

                0x20: "=cc",
                0x21: "=cc",
                0x22: "=cc",
                0x23: "=cc",
                0x24: "=cc",
                0x25: "=cc",
                0x26: "=cc",
                0x27: "=cc",
                0x28: "=cc",
                0x29: "=cc",
                0x2A: "=cc",
                0x2B: "=cc",
                0x2C: "=cc",
                0x2D: "=cc",
                0x2E: "=cc",
                0x2F: "=cc",

                0x30: "=cc",
                0x31: "=cc",
                0x32: "=cc",
                0x33: "=cc",
                0x34: "=cc",
                0x35: "=cc",
                0x36: "=cc",
                0x37: "=cc",

                0x60: "=cc",
                0x61: "=cc",
                0x62: "=cc",
                0x63: "=cc",
                0x64: "=cc",

                0x70: "=BBBBBBBB"
            }

            self.response_bytes = {
                0x01: 4,
                0x02: 2,
                0x03: 1,
                0x04: 1,
                0x05: 1,
                0x07: 12,
                0x08: 2,
                0x09: 12,
                0x0e: 2,
                0x0f: 12,

                0x10: 2,
                0x11: 2,
                0x12: 2,
                0x13: 2,
                0x14: 2,
                0x15: 2,
                0x16: 2,
                0x17: 2,
                0x18: 2,
                0x19: 2,  # fixed 18.12
                0x1A: 2,
                0x1B: 2,  # fixed 18.12

                0x20: 2,
                0x21: 2,
                0x22: 2,
                0x23: 2,
                0x24: 2,
                0x25: 2,
                0x26: 2,
                0x27: 2,
                0x28: 2,
                0x29: 2,
                0x2A: 2,
                0x2B: 2,
                0x2C: 2,
                0x2D: 2,
                0x2E: 2,
                0x2F: 2,

                0x30: 2,
                0x31: 2,
                0x32: 2,
                0x33: 2,
                0x34: 2,
                0x35: 2,
                0x36: 2,
                0x37: 2,

                0x60: 2,
                0x61: 2,
                0x62: 2,
                0x63: 2,
                0x64: 2,

                0x70: 8
            }

    def send(self, cmd, args):
        self.mutex.acquire()
        successfully, values = self.send_command(cmd, args)
        self.mutex.release()
        print( 'Got response: ' + str(values))
        return successfully, values

    def send_command(self, cmd, args, n_repeats=5):
        for i in range(n_repeats):
            try:
                return self.pure_send_command(cmd, args)
            except Exception as exc:
                if i == n_repeats - 1:
                    pass
                    # print( 'Exception:\t' + str(exc))
                    # print( 'At time:\t' + str(time.time))
                    # print('cmd:' + str(cmd) + '; args:' + str(args))
                    # print( '--------------------------')
        return False, None

    def parse_data(self, data):
        data_splitted = data.split()
        # print(data_splitted)
        id = data_splitted[0]
        try:
            cmd = int(data_splitted[1])
        except ValueError as e:
            cmd = int(data_splitted[1], 16)
        args_dict = {'c': str, 'B': int, 'f': float}
        # [print('t: ', t, '\ns: ', s) for t, s in zip(self.pack_format[cmd][1:], data_splitted[2:])]
        args = [args_dict[t](s) for t, s in zip(self.pack_format[cmd][1:], data_splitted[2:])]
        return id, cmd, args

    def pure_send_command(self, cmd, args):
        # Clear buffer
        self.ser.reset_output_buffer()
        self.ser.reset_input_buffer()
        # Sending command
        if args != None:
            pass
            # print('Send the msg:' + str(cmd) + ' ' + str(args))
        else:
            pass
            # print('Send the msg:' + str(cmd))
        msg = bytearray([cmd])
        if args:
            parameters = bytearray(struct.pack(self.pack_format[cmd], *args))
            msg += parameters
        self.ser.write(msg)
        # print( "MSG to send")
        # print (msg)
        response = self.ser.read(self.response_bytes[cmd])
        if len(response) == 0:
            raise Exception("No data received")
        values = struct.unpack(self.unpack_format[cmd], response)
        return True, values

if __name__ == "__main__":
    serial_port = "\\.\COM4"
    stm = STMprotocol(serial_port)

    while True:
        # set debug mode
        data = "0 0x02"
        id, cmd, args = stm.parse_data(data)
        temp, value = stm.send(cmd, args)
        if value != None: break
    cmd_line = ''

    cmd_line = "1 0x0E 0 0 0"
    id, cmd, args = stm.parse_data(cmd_line)
    temp, value = stm.send(cmd, args)

    flag = False
    t_start = 0
    counter = 0

    while True: 
        cmd_line = input ("Enter cmd code in HEX or 'end'.\n")
        # cmd_line = "1 0x70"
        # if counter == 0:
        #     distance = 20
        #     cmd_line = "1 0x16 20 0"
        # elif counter == 1:
        #     distance = 100
        #     cmd_line = "1 0x16 100 0"
        # elif counter == 2:
        #     distance = 170
        #     cmd_line = "1 0x16 170 0"
        # elif counter == 3:
        #     distance = 230
        #     cmd_line = "1 0x16 230 0"        

        if cmd_line == 'end': break
        id, cmd, args = stm.parse_data(cmd_line)
        temp, value = stm.send(cmd, args)

        # id, cmd, args = stm.parse_data("0 0x70")
        # temp, value = stm.send(cmd, args)
        # while abs(distance - value[7]) > 5:
        #     id, cmd, args = stm.parse_data("0 0x70")
        #     temp, value = stm.send(cmd, args)
        
        # print("stated")
        # for j in range (10000):
        #     id, cmd, args = stm.parse_data("0 0x70")
        #     temp, value = stm.send(cmd, args)

        # counter += 1
        # if counter > 3:
        #     counter = 0
    cmd_line = "1 0x08 0 0 0"
    id, cmd, args = stm.parse_data(cmd_line)
    temp, value = stm.send(cmd, args)
    time = time.time() - t_start

    cmd_line = "1 0x0F"
    id, cmd, args = stm.parse_data(cmd_line)
    temp, value = stm.send(cmd, args)

    print("TIME: ", time)
    print("X: ", value[0], "; Y: ", value[1], "; Q: ", value[2])
