#ifndef _DEV_MAP_H_
#define _DEV_MAP_H_

#define NUMBER_OF_DYNAMIXELS            9
#define NUMBER_OF_PROX_SENSORS          3
#define PAWS_NUMBER 5

#endif //_DEV_MAP_H_
