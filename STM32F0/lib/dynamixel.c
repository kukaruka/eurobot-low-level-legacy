#include "dynamixel.h"
#include "gpio_map.h"
#include "peripheral.h"
#include "err_manager.h"

#include <string.h>
#include "stm32f0xx_ll_rcc.h"
#include "stm32f0xx_ll_bus.h"

/*
 * Send command
 */
static void dyn_send_cmd(uint8_t *buff, int len)
{
    int i = 0;
    __disable_irq();
    LL_USART_DisableDirectionRx(DYNAMIXEL_USART);
    LL_USART_EnableDirectionTx(DYNAMIXEL_USART);
    LL_USART_ClearFlag_TC(DYNAMIXEL_USART);
    while (len--)
    {
        while (!LL_USART_IsActiveFlag_TXE(DYNAMIXEL_USART))
            ;
        LL_USART_TransmitData8(DYNAMIXEL_USART, buff[i++]);
    }
    while (!LL_USART_IsActiveFlag_TC(DYNAMIXEL_USART))
        ;
    LL_USART_DisableDirectionTx(DYNAMIXEL_USART);
    LL_USART_EnableDirectionRx(DYNAMIXEL_USART);
    __enable_irq();
    return;
}

static void dyn_delay(uint32_t i)
{
    while (i--)
        ;
    return;
}

void dyn_set_paws_angle(uint8_t *angles)
{
    uint8_t tx[9];
    uint8_t packet_length = 5;

    for (uint8_t id = 1, j = 0; id < PAWS_NUMBER + 1; ++id, j = j + 2)
    {
        tx[0] = 0xff;
        tx[1] = 0xff;
        tx[3] = packet_length;
        tx[4] = 0x03; //instruction (write data)
        tx[5] = 0x1e; //goal position
        tx[2] = id;
        uint8_t highByte = angles[j];
        uint8_t lowByte = angles[j + 1];
        tx[6] = lowByte;
        tx[7] = highByte;
        uint16_t angle = (uint8_t)highByte;
        angle = angle << 8;
        angle += lowByte;
        uint16_t check_sum = 0x00;
        for (uint8_t ind = 2; ind < 8; ++ind)
            check_sum += tx[ind];
        uint8_t chsum = (uint8_t)((~check_sum) & 0xff);
        tx[8] = chsum;
        dyn_send_cmd(tx, 9);
        dyn_delay(48000000 / 1000);
    }
}

void dyn_change_man_pose(uint8_t *angles)
{
    uint8_t tx[9];
    uint8_t packet_length = 5;
    // add comment
    


    for (uint8_t id = 1, j = 0; id < 3; ++id, j = j + 2)
    {
        tx[0] = 0xff;
        tx[1] = 0xff;
        tx[2] = id;
        tx[3] = packet_length;
        tx[4] = 0x03; //instruction (write data)
        tx[5] = 0x1e; //goal position
        uint8_t highByte = angles[j];
        uint8_t lowByte = angles[j + 1];
        tx[6] = lowByte;
        tx[7] = highByte;

        uint16_t angle = (uint8_t)highByte;
        angle = angle << 8;
        angle += lowByte;

        uint16_t check_sum = 0x00;
        for (uint8_t ind = 2; ind < 8; ++ind)
            check_sum += tx[ind];
        uint8_t chsum = (uint8_t)((~check_sum) & 0xff);
        tx[8] = chsum;
        dyn_send_cmd(tx, 9);
        dyn_delay(48000000 / 1000);
    }
}

void dyn_man_pose(uint8_t *angles)
{
    uint8_t tx[9];
    uint8_t packet_length = 5;
    // add comment
    uint8_t ids[2];
    ids[0] = 0x11;
    ids[1] = 0x12;


    for (uint8_t id = 1, j = 0; id < 3; ++id, j = j + 2)
    {
        tx[0] = 0xff;
        tx[1] = 0xff;
        tx[2] = ids[id];
        tx[3] = packet_length;
        tx[4] = 0x03; //instruction (write data)
        tx[5] = 0x1e; //goal position
        uint8_t highByte = angles[j];
        uint8_t lowByte = angles[j + 1];
        tx[6] = lowByte;
        tx[7] = highByte;

        uint16_t angle = (uint8_t)highByte;
        angle = angle << 8;
        angle += lowByte;

        uint16_t check_sum = 0x00;
        for (uint8_t ind = 2; ind < 8; ++ind)
            check_sum += tx[ind];
        uint8_t chsum = (uint8_t)((~check_sum) & 0xff);
        tx[8] = chsum;
        dyn_send_cmd(tx, 9);
        dyn_delay(48000000 / 1000);
    }
}

void dyn_move_cart(uint8_t *speed)
{
    uint8_t tx[9];
    uint8_t packet_length = 5;

    tx[0] = 0xff;
    tx[1] = 0xff;
    tx[2] = 5;
    tx[3] = packet_length;
    tx[4] = 0x03; //instruction (write data)
    tx[5] = 0x20; //set speed 0x20; 
    uint8_t highByte = speed[0];
    uint8_t lowByte = speed[1];
    tx[6] = lowByte;
    tx[7] = highByte;

    uint16_t spd = (uint8_t)highByte;
    spd = spd << 8;
    spd += lowByte;

    uint16_t check_sum = 0x00;
    for (uint8_t ind = 2; ind < 8; ++ind)
        check_sum += tx[ind];
    uint8_t chsum = (uint8_t)((~check_sum) & 0xff);
    tx[8] = chsum;
    dyn_send_cmd(tx, 9);
    dyn_delay(48000000 / 1000);
    return;
}

void dyn_set_init_pos(uint8_t *angles)
{
    uint8_t tx[9];
    uint8_t packet_length = 5;
    tx[0] = 0xff;
    tx[1] = 0xff;
    tx[3] = packet_length; //goal position
    tx[4] = 0x03;          //instruction (write data)
    tx[5] = 0x1e;
    for (uint8_t id = 1, j = 0; id < NUMBER_OF_DYNAMIXELS + 1; ++id, j = j + 2)
    {
        tx[2] = id;
        uint8_t highByte = angles[j];
        uint8_t lowByte = angles[j + 1];
        tx[6] = lowByte;
        tx[7] = highByte;
        uint16_t angle = (uint8_t)highByte;
        angle = angle << 8;
        angle += lowByte;

        if ((angle != DYN_PAWS_OPEN) ^ (angle != DYN_PAWS_CLOSE))
            continue;

        uint16_t check_sum = 0x00;
        for (uint8_t ind = 2; ind < 8; ++ind)
            check_sum += tx[ind];
        uint8_t chsum = (uint8_t)((~check_sum) & 0xff);
        tx[8] = chsum;
        dyn_send_cmd(tx, 9);
        dyn_delay(48000000 / 1000);
    }
}

void dyn_set_doors_angle(uint8_t *angles)
{
    uint8_t tx[9];
    uint8_t packet_length = 5;
    for (uint8_t id = PAWS_NUMBER + 1; id < NUMBER_OF_DYNAMIXELS + 1; id++)
    {
        tx[0] = 0xff;
        tx[1] = 0xff;
        tx[3] = packet_length;
        tx[4] = 0x03; //instruction (write data)
        tx[5] = 0x1e; //goal adres position
        tx[2] = id;
        uint8_t highByte = angles[2 * id - 2];
        uint8_t lowByte = angles[2 * id - 1];
        tx[6] = lowByte;
        tx[7] = highByte;
        uint16_t angle = (uint8_t)highByte;
        angle = angle << 8;
        angle += lowByte;
        uint16_t check_sum = 0x00;
        for (uint8_t ind = 2; ind < 8; ++ind)
            check_sum += tx[ind];
        uint8_t chsum = (uint8_t)((~check_sum) & 0xff);
        tx[8] = chsum;
        dyn_send_cmd(tx, 9);
        dyn_delay(48000000 / 1000);
    }
}

static void dyn_set_speed(uint8_t *speeds)
{
    static const uint8_t DYN_SET_ANGLE_CMD_LEN = 9;
    int i = 0;
    int j = 0;
    for (i = 0, j = 0; i < NUMBER_OF_DYNAMIXELS; i++, j = j + 2)
    {
        uint8_t highByte = speeds[j];
        uint8_t lowByte = speeds[j + 1];
        uint8_t crc = (i + 1) + 0x05 + 0x03 + 0x20 + lowByte + highByte;
        uint8_t tx[] = {0xff, 0xff, i + 1, 0x05, 0x03, 0x20, lowByte,
                        highByte, ~crc};
        dyn_send_cmd(tx, DYN_SET_ANGLE_CMD_LEN);
        dyn_delay(48000000 / 1000);
    }
    return;
}

static void dyn_disable_torque(void)
{
    static const uint8_t DYN_DISABLE_TORQUE_CMD_LEN = 8;
    uint8_t crc = 0xfe + 0x04 + 0x03 + 0x18 + 0x00;
    uint8_t tx[] = {0xff, 0xff, 0xfe, 0x04, 0x03, 0x18, 0x00,
                    ~crc};
    dyn_send_cmd(tx, DYN_DISABLE_TORQUE_CMD_LEN);
    dyn_delay(48000000 / 1000);
    return;
}

void dynamixel_init()
{
    /*
     * Initialization code
     * Setting USART pin
     */
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
    /*
     * USART1_TX_RX setting
     */
    LL_GPIO_SetPinMode(DYNAMIXEL_USART_PORT, DYNAMIXEL_USART_PIN,
                       LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetAFPin_8_15(DYNAMIXEL_USART_PORT, DYNAMIXEL_USART_PIN,
                          DYNAMIXEL_USART_PIN_AF);
    LL_GPIO_SetPinOutputType(DYNAMIXEL_USART_PORT, DYNAMIXEL_USART_PIN,
                             LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinPull(TERM_USART_TX_PORT, TERM_USART_TX_PIN,
                       LL_GPIO_PULL_DOWN);
    LL_GPIO_SetPinSpeed(DYNAMIXEL_USART_PORT, DYNAMIXEL_USART_PIN,
                        LL_GPIO_SPEED_FREQ_HIGH);
    /*
     * USART Setting
     */
    LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_USART1);
    LL_USART_SetParity(DYNAMIXEL_USART, DYNAMIXEL_USART_PARITY);
    LL_USART_SetDataWidth(DYNAMIXEL_USART, DYNAMIXEL_USART_DATA_WIDTH);
    LL_USART_SetStopBitsLength(DYNAMIXEL_USART, DYNAMIXEL_USART_STOPBITS);
    LL_USART_SetBaudRate(DYNAMIXEL_USART, SystemCoreClock,
                         DYNAMIXEL_USART_OVERSAMPL,
                         DYNAMIXEL_USART_BAUDRATE);
    LL_USART_EnableHalfDuplex(DYNAMIXEL_USART);
    LL_USART_DisableDirectionTx(DYNAMIXEL_USART);
    LL_USART_EnableDirectionRx(DYNAMIXEL_USART);
    LL_USART_Enable(DYNAMIXEL_USART);
    /*
     * Configure dynamixel reset pin
     */
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOC);
    LL_GPIO_SetPinMode(DYN_RESET_PORT, DYN_RESET_PIN, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(DYN_RESET_PORT, DYN_RESET_PIN,
                             LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetOutputPin(DYN_RESET_PORT, DYN_RESET_PIN);
    return;
}

void dyn_manager(void *args)
{
    uint8_t *cmd_args = (uint8_t *)args;
    dyn_ctrl_t *dyn_ctrl = (dyn_ctrl_t *)&(cmd_args[1]);
    uint8_t cmd_code = cmd_args[0];
    switch (cmd_code)
    {
    case 0:
        // dyn_set_angle(dyn_ctrl->id, dyn_ctrl->angle);
        // break;
    case 1:
        // dyn_set_speed(&cmd_args[1]);
        // break;
    case 2:
        dyn_set_init_pos(&cmd_args[1]);
        break;
    case 3:
        dyn_set_doors_angle(&cmd_args[1]);
        break;
    case 4:
        dyn_set_paws_angle(&cmd_args[1]);
        break;
    case 5:
        dyn_change_man_pose(&cmd_args[1]);
        break;
    case 6:
        dyn_move_cart(&cmd_args[1]);
        break;
    case 7:
        dyn_man_pose(&cmd_args[1]);
        break;
    default:
        break;
    }
    return;
}
