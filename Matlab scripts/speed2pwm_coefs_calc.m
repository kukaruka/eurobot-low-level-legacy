L = 0.123;                 % deistance from wheel center to [0;0]
k_corr = 1;                %1.4529375;
R = 0.03;                  % wheel radius
inp_speed = [0.1; 0.0; 0.0]; % input speed in global coordinate system 
gearhead = 28;             % gear ratio

%% Kinematics
A = [ cos(pi/6) cos(pi/3) L;
     -cos(pi/6) cos(pi/3) L;
     0          -1        L];
k = 1/(k_corr*R);
B = inv(A)/k; % inverse odometry matrix
A = A*k;      % forward kinematics matrix
    
disp("Forward odometry matrix :")
format long
disp(A)
disp("Inverse odometry matrix :")
disp(B)
%% PWM calc
format shortg
Alin = [A(1:3, 1:2) [0;0;0]];
Arot = [zeros(3,2) A(:,3)];

req_wheel_rot_speed = (Alin*inp_speed + Arot*inp_speed) % [rad/s]
req_motor_rot_speed = gearhead * req_wheel_rot_speed;  % [rad/s]
req_motor_rpm = req_motor_rot_speed * 60/(2*pi) % [rev per minute]

min_rpm = 0;
max_rpm = 6000;
min_pwm = 0.12;
max_pwm = 0.9;

% load('pwm.mat')
% pwm(:,2) = pwm(:,2).*100;
% pwm(:,3) = pwm(:,3).*100;
% pwm(:,4) = pwm(:,4).*100;
% pwm_coef = abs(pwm(:,2)\pwm(:,1)); % kinematics when k_corr = 1.4529375;

pwm_coef = (gearhead*60/(2*pi))*(max_pwm-min_pwm)/(max_rpm-min_rpm)
pwm_out = req_wheel_rot_speed * pwm_coef + [min_pwm; min_pwm; -min_pwm];
