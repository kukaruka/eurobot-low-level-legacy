L = 0.123;
k_corr = 1; %1.4529375;
R = 0.03;
A = [ cos(pi/6) cos(pi/3) L;
     -cos(pi/6) cos(pi/3) L;
     0          -1        L];
k = 1/(k_corr*R);
B = inv(A)/k;

disp("Forward odometry matrix :")
format long
disp(A*k)
disp("Inverse odometry matrix :")
disp(B)